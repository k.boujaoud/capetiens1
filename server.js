var express = require('express');
var server = express();
var bodyParser = require("body-parser");
var fs = require("fs");
var jsonfile = require("jsonfile");


server.set('view engine','ejs');
server.use(bodyParser.urlencoded({ extended: true }));


server.get("/", function(req,res){
    res.render("pages/homepage.ejs");
})

server.get("/hugues", function(req,res){
    res.render("pages/hugues.ejs");
})
server.get("/louis", function (req, res) {
    res.render("pages/louis.ejs");
})
server.get("/auguste", function (req, res) {
    res.render("pages/auguste.ejs");
})
server.get("/lebel", function (req, res) {
    res.render("pages/lebel.ejs");
})
server.get("/formulaire", function (req, res) {
    res.render("pages/formulaire.ejs");
})
server.post('/formulaire', function (req, res) {
    
    let adherent = req.body;

    let data = JSON.stringify(adherent, null, 2);

    fs.appendFile('adherents.json', data, (err) => {
        if (err) throw err;
        console.log(data);
    })
        res.render("pages/formulaire.ejs");

})

server.listen("3000", function(){
    console.log("Ton serveur est bien en ligne !");
})

server.use(express.static("public"));

