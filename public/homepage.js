//FONCTION POUR LA NAVBAR RESPONSIVE

$(".menuburger").click(function () {
    $("#navbar").slideToggle();
});

//FONCTION POUR LA NAVBAR RESPONSIVE

$(".dropdown").click(function () {
    $(".dropdown-content").slideToggle();
});


function Horloge() {
    var today = new Date(); // récupération de l'heure client
    var h = today.getHours(); // extraction de l'heure
    var m = today.getMinutes(); // extraction des minutes
    var s = today.getSeconds(); // extraction des secondes

    // h = checkTime(h);           // appel de la fonction ci-dessous
    // m = checkTime(m);           // pour
    // s = checkTime(s);           // formattage de l'affichage : ajout d'un zéro
    let i = m % 2;

    document.getElementById('montre').innerHTML = "<figure style=color:#1C1D21;>" + h + ":" + m + ":" + s + "</figure>";

    if (i === 0) {
        document.getElementById('montre').innerHTML = "<figure style=color:#a56c4a;>" + h + ":" + m + ":" + s + "</figure>";
    }

}
setInterval(Horloge, 1000); // rappel de la fonction toutes les secondes



// Get the container element
var header = document.getElementById("navbar");

// Get all buttons with class="btn" inside the container
var btns = header.getElementsByClassName("lien");

// Loop through the buttons and add the active class to the current/clicked button
for (var i = 0; i < btns.length; i++) {
    btns[i].addEventListener("click", function () {
        var current = document.getElementsByClassName("active");
        current[0].className = current[0].className.replace(" active", "");
        this.className += " active";
    });
}



$(function () {
    /**
     * Smooth scrolling to page anchor on click
     **/
    $("a[href*='#']:not([href='#'])").click(function () {
        if (
            location.hostname == this.hostname &&
            this.pathname.replace(/^\//, "") == location.pathname.replace(/^\//, "")
        ) {
            var anchor = $(this.hash);
            anchor = anchor.length ? anchor : $("[name=" + this.hash.slice(1) + "]");
            if (anchor.length) {
                $("html, body").animate({
                    scrollTop: anchor.offset().top
                }, 1500);
            }
        }
    });
});
// Get the container element
var header = document.getElementById("navbar");

// Get all buttons with class="btn" inside the container
var btns = header.getElementsByClassName("lien");

// Loop through the buttons and add the active class to the current/clicked button
for (var i = 0; i < btns.length; i++) {
    btns[i].addEventListener("click", function () {
        var current = document.getElementsByClassName("active");
        current[0].className = current[0].className.replace(" active", "");
        this.className += " active";
    });
}
$(function () {
    $(window).scroll(function () { //Fonction appelée quand on descend la page
        if ($(this).scrollTop() > 200) { // Quand on est à 200pixels du haut de page,
            $('#scrollUp').css('right', '10px'); // Replace à 10pixels de la droite l'image
        } else {
            $('#scrollUp').removeAttr('style'); // Enlève les attributs CSS affectés par javascript
        }
    });
});

// IMAGE SLIDES & CIRCLES ARRAYS, & COUNTER
var imageSlides = document.getElementsByClassName('imageSlides');
var circles = document.getElementsByClassName('circle');
var leftArrow = document.getElementById('leftArrow');
var rightArrow = document.getElementById('rightArrow');
var counter = 0;

// HIDE ALL IMAGES FUNCTION
function hideImages() {
    for (var i = 0; i < imageSlides.length; i++) {
        imageSlides[i].classList.remove('visible');
    }
}

// REMOVE ALL DOTS FUNCTION
function removeDots() {
    for (var i = 0; i < imageSlides.length; i++) {
        circles[i].classList.remove('dot');
    }
}

// SINGLE IMAGE LOOP/CIRCLES FUNCTION
function imageLoop() {
    var currentImage = imageSlides[counter];
    var currentDot = circles[counter];
    currentImage.classList.add('visible');
    removeDots();
    currentDot.classList.add('dot');
    counter++;
}

// LEFT & RIGHT ARROW FUNCTION & CLICK EVENT LISTENERS
function arrowClick(e) {
    var target = e.target;
    if (target == leftArrow) {
        clearInterval(imageSlideshowInterval);
        hideImages();
        removeDots();
        if (counter == 1) {
            counter = (imageSlides.length - 1);
            imageLoop();
            imageSlideshowInterval = setInterval(slideshow, 10000);
        } else {
            counter--;
            counter--;
            imageLoop();
            imageSlideshowInterval = setInterval(slideshow, 10000);
        }
    } else if (target == rightArrow) {
        clearInterval(imageSlideshowInterval);
        hideImages();
        removeDots();
        if (counter == imageSlides.length) {
            counter = 0;
            imageLoop();
            imageSlideshowInterval = setInterval(slideshow, 10000);
        } else {
            imageLoop();
            imageSlideshowInterval = setInterval(slideshow, 10000);
        }
    }
}

leftArrow.addEventListener('click', arrowClick);
rightArrow.addEventListener('click', arrowClick);


// IMAGE SLIDE FUNCTION
function slideshow() {
    if (counter < imageSlides.length) {
        imageLoop();
    } else {
        counter = 0;
        hideImages();
        imageLoop();
    }
}

// SHOW FIRST IMAGE, & THEN SET & CALL SLIDE INTERVAL
setTimeout(slideshow, 1000);
var imageSlideshowInterval = setInterval(slideshow, 10000);